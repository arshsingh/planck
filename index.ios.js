'use strict';

import React from 'react-native';
import Main from './app/containers';


var {
  AppRegistry
} = React;

AppRegistry.registerComponent('planck', () => Main);
