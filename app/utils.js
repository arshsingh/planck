'use strict';

import _ from 'lodash';

import categories from './categories';


export function getLabelForCategory(id) {
  for (var i = 0; i < categories.length; i++) {
    let cat = categories[i];
    if (cat.id === id) return cat.label;

    // A sub-category's id will be a superset of it's parent's id
    // No need to check sub-categories if this condition does not hold
    if (_.startsWith(id, cat.id) && !_.isEmpty(cat.children)) {
      for (var k = 0; k < cat.children.length; k++) {
        let subCat = cat.children[k];
        if (subCat.id === id) return cat.label + ' - ' + subCat.label;
      }
    }
  }
}
