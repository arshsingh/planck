'use strict';

import { combineReducers } from 'redux';
import _ from 'lodash';

import * as Actions from './actions';


/*
 * All papers to display in the main list view
 */
function papers(state = [], action) {
  if (action.status === Actions.SUCCESS) {
    switch (action.type){
      case Actions.HYDRATE:
        state = action.papers;
        break;
      case Actions.FETCH_PAPERS:
        state = action.papers.concat(
            _.take(state, 30 - action.papers.length));
        break;
      case Actions.DOWNLOAD_PAPER:
        state = _.map(state, (p) => {
          if (p.id === action.paper.id) p.file = action.file;
          return p;
        })
        break;
    }
  }

  return state;
}

/*
 * Currently/Last opened paper
 */
function detail(state = {}, action) {
  switch (action.type) {
    case Actions.SET_DETAIL:
      state = action.paper
  }
  return state;
}

/*
 * All on-going downloads. These include temp
 * downloads as well.
 */
function downloading(state = [], action) {
  if (action.type === Actions.DOWNLOAD_PAPER) {
    const { save, paper, progress } = action;

    switch (action.status) {
      case Actions.INITIATED:
        state.push({ paper, save, progress });
        break;
      case Actions.PROGRESS:
        state = _.map(state, (d) => {
          if (d.paper.id === paper.id) d.progress = progress;
          return d;
        })
        break;
      case Actions.FAILED:
      case Actions.SUCCESS:
        state = _.reject(state, 'paper.id', paper.id);
        break;
    }
  }

  return state;
}

const compositeReducer = combineReducers({
  papers, detail, downloading
});

export default compositeReducer;
