'use strict';

import db from 'react-native-simple-store';
import moment from 'moment';
import _ from 'lodash';
import RNFS from 'react-native-fs';

import { Paper } from './arxiv';
import categories from './categories';


// action statuses
export const SUCCESS = 'success';
export const INITIATED = 'initiated';
export const FAILED = 'failed';
export const ABORTED = 'aborted';
export const PROGRESS = 'PROGRESS';


// action types
export const HYDRATE = 'HYDRATE';
export const FETCH_PAPERS = 'FETCH_PAPERS';
export const SET_DETAIL = 'SET_DETAIL';
export const DOWNLOAD_PAPER = 'DOWNLOAD_PAPER';


export function hydrate() {
  return dispatch => {
    dispatch({ type: HYDRATE, status: INITIATED });

    // load the items from db to store
    db.get('papers').then(_papers => {
      const papers = _.map(_papers, obj => new Paper(obj));

      dispatch({
        type: HYDRATE,
        papers: papers,
        status: SUCCESS
      });
    });

    // fetch new papers from server and save to db
    dispatch(fetchPapers({ save: true }));
  }
}

export function fetchPapers(options = {}) {
  return dispatch => {
    dispatch({ type: FETCH_PAPERS, status: INITIATED });

    db.get('config').then(_conf => {
      const conf = _conf || {};

      // fetch only after 10 mins unless manually triggered (scroll-to-refresh)
      let lastFetch = conf.lastFetch ? new Date(conf.lastFetch) : new Date(1970, 0, 1);
      if ((new Date() - lastFetch) < 600000 && !options.force) {
        return dispatch({ type: FETCH_PAPERS, status: ABORTED });
      }

      let cats = categories;
      if (!_.isEmpty(options.categories)) {
        cats = options.categories;
      } else if (!_.isEmpty(conf.categories)) {
        cats = conf.categories;
      }

      let url = (
        'http://export.arxiv.org/api/query?sortBy=submittedDate' +
        '&sortOrder=descending&max_results=35&search_query=');
      url += _.map(cats, (cat) => `cat:${cat.query}`).join('+OR+');
      if (conf.lastFetch) {
        url += (
          '+AND' + 'lastUpdateDate:[' +
          moment(conf.lastFetch).format('YYYYMMDDhmmss') +
          '+TO+' + moment(new Date()).format('YYYYMMDDhmmss'));
      }

      fetch(url)
        .then(response => response.text())
        .then(xml => Paper.fromXML(xml))
        .then(papers => {
          dispatch({
            type: FETCH_PAPERS,
            papers: papers,
            status: SUCCESS
          });

          if (options.save === true) {
            // if there are n fetched papers, the cache can be replaced
            // otherwise include existing papers to make the total n
            db.get('papers').then(_old => {
              let old = _.map(_old, obj => new Paper(obj));
              let newPapers = papers.concat(_.take(old, 30 - papers.length));
              db.save('papers', newPapers);
            });

            db.save('config', { ...conf, lastFetch: new Date() });
          }
        })
        .catch(function(error) {
          dispatch({ type: FETCH_PAPERS, status: FAILED, error: error });
        });
    });
  }
}

export function setDetail(paper) {
  return {
    type: SET_DETAIL,
    paper: paper
  }
}


/*
 * Download a paper for offline viewing. If the save param
 * is set to false, this will be saved in the /tmp dir to be
 * deleted later (for Android. On iOS the default WebView
 * plugin will download the remote file)
 */
export function downloadPaper(paper, save=false) {
  return dispatch => {
    const action = {
      type: DOWNLOAD_PAPER,
      paper: paper,
      save: save
    };

    dispatch({ ...action, status: INITIATED });

    let url = (
      RNFS.DocumentDirectoryPath
      + (save ? '' : '/tmp')
      + '/' + paper.id + '.pdf'
    )

    RNFS.mkdir(RNFS.DocumentDirectoryPath + '/tmp').then(() => {
      RNFS.downloadFile(paper.pdf, url, null, (progress) => {
        dispatch({ ...action, status: PROGRESS, progress: progress });
      }).then(() => {
        dispatch({  ...action, file: url, status: SUCCESS });

        if (save) {
          db.get('downloaded').then((downloaded = []) => {
            db.save('downloaded', [ ...downloaded, { ...paper, _file: url } ]);
          });

          // update the paper in the main list as well
          db.get('papers').then((papers = []) => {
            db.save('papers', _.map(papers, p => (
              p.id === paper.id ? { ...p, file: url } : p
            )))
          });
        }
      }).catch(() => dispatch({  ...action, status: FAILED }));
    });
  }
}
