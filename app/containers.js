'use strict';

import React, { Component, Text, Navigator } from 'react-native';
import { Provider, connect } from 'react-redux/native';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import _ from 'lodash';

import { hydrate } from './actions';
import compositeReducer from './reducers';
import List from './components/list';
import Detail from './components/detail';
import Reader from './components/reader';


const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware
)(createStore);

const store = createStoreWithMiddleware(compositeReducer);

/*
 * app container
 */
class UnconnectedApp extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { detail, papers, downloading, dispatch} = this.props;

    return <Navigator
      initialRoute={{name: 'Papers', index: 0, id: List.sceneId}}
      style={{backgroundColor: '#333'}}
      renderScene={(route, navigator) => {
        let props = {
          ...route.props,
          dispatch, navigator
        };

        switch (route.id) {
          case List.sceneId:
            return <List {...props} papers={papers}/>
          case Detail.sceneId:
            return <Detail {...props} paper={detail}/>
          case Reader.sceneId:
            return (
              <Reader {...props}
                paper={detail}
                downloading={_.find(downloading, 'paper.id', detail.id)}/>
            );
        }
      }}
    />
  }
}
let App = connect(state => state)(UnconnectedApp);  // FIXME: enable es7.decorators


/*
 * main container
 */
export default class Main extends Component {
  render() {
    return <Provider store={store}>
      {() => <App />}
    </Provider>
  }
}


// load data from AsyncStore
store.dispatch(hydrate())
