'use strict';

import _ from 'lodash';
import xml2js from 'xml2js';

import categories from './categories';


export class Paper {
  constructor(data) {
    this.id = data.id;
    this.title = data.title;
    this.summary = data.summary;
    this.authors = data.authors;
    this.categories = data.categories;
    this.primaryCategory = data.primaryCategory;
    this.published = data.published;
    this.updated = data.updated;
    this.comment = data.comment;
    this.pdf = data.pdf;
  }

  /*
   * takes the arXiv ATOM feed as a string and
   * returns a promise to an array of Paper instances
   */
  static fromXML(xml) {
    const self = this;
    return new Promise((resolve, reject) => {
      let parser = new xml2js.Parser({
        mergeAttrs: true
      });
      parser.parseString(xml, (err, result) => {
        if (err) reject(err);
        
        resolve(_.map(result.feed.entry, (data) => {
          let item = self._normalizeItem(data);
          return new Paper(item);
        }));
      });
    });
  }

  static _normalizeItem(data) {
    let item = {};

    item.id = _.last(_.first(data.id).split('/'));
    item.title = _.first(data.title).replace(/[\s]+/g, ' ');  // remove newlines
    item.summary = _.trim(_.first(data.summary).replace(/[\s]+/g, ' '));
    item.authors = _.flatten(_.pluck(data.author, 'name'));
    item.categories = _.flatten(_.pluck(data.category, 'term'));
    item.primaryCategory = _.first(_.first(data["arxiv:primary_category"]).term);
    item.published = new Date(_.first(data.published));
    item.updated = new Date(_.first(data.updated));
    item.pdf = _.first(_.find(data.link, 'type', ['application/pdf']).href);

    if (data['arxiv:comment']){
      item.comment = _.trim(_.first(data['arxiv:comment'])._.replace(/[\s]+/g, ' '));
    }

    return item;
  }

  getColor() {
    let defaultColor = '#666';
    let primary = this.primaryCategory || _.first(this.categories);

    for (var i = 0; i < categories.length; i++) {
      let cat = categories[i];
      if (cat.id === primary) {
        return cat.color || defaultColor;
      }

      // A sub-category's id will be a superset of it's parent's id
      // No need to check sub-categories if this condition does not hold
      if (_.startsWith(primary, cat.id) && !_.isEmpty(cat.children)) {
        for (var k = 0; k < cat.children.length; k++) {
          let subCat = cat.children[k];
          if (subCat.id === primary) {
            return subCat.color || cat.color || defaultColor;
          }
        }
      }
    }

    return defaultColor;
  }
}
