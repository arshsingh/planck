'use strict';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  list: {
    flex: 1
  },
  item: {
    padding: 20,
    backgroundColor: '#FFF',
    borderBottomColor: '#E3E3E3',
    borderBottomWidth: 0.3,
    borderLeftWidth: 5,
  },
  itemTitle: {
    color: '#666',
    marginBottom: 5,
    fontSize: 15
  },
  itemSubtext: {
    color: '#AAA',
    fontSize: 12
  }
});
