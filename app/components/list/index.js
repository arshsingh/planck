'use strict';

import React from 'react-native';
import _ from 'lodash';
import NavBar from 'react-native-material-navbar';

import { setDetail } from '../../actions';
import Detail from '../detail/index';
import Item from './item';
import styles from './styles';

const {
  Component,
  ListView,
  TouchableHighlight,
  View,
  ScrollView
} = React;


export default class List extends Component {
  static get sceneId() { return 'list'; }

  onOpen(paper) {
    const { navigator, dispatch } = this.props;
    dispatch(setDetail(paper));
    navigator.push({
      name: _.trunc(paper.title, 35),
      id: Detail.sceneId
    });
  }

  render() {
    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    let listItems = ds.cloneWithRows(this.props.papers);

    return <View style={styles.container}>
      <NavBar statusBarColorAndroid='#333333'/>
      <ListView style={styles.list}
        dataSource={listItems}
        renderRow={(data) =>
          <Item 
            paper={data}
            onOpen={() => this.onOpen(data)}
          />
        }
      />
    </View>
  }
}
