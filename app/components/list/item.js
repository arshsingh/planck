'use strict';

import React from 'react-native';
import _ from 'lodash';

import styles from './styles';

const {
  Component,
  View,
  Text,
  TouchableHighlight
} = React;


export default class Item extends Component {
  _getAuthorsDisplay(authors) {
    let [head, tail] = _.chunk(authors, 2);
    return head.join(', ') + (_.size(tail) ? ' et al' : '')
  }

  render() {
    const { paper } = this.props;

    return <TouchableHighlight
              onPress={this.props.onOpen}
              underlayColor='#E3E3E3'>
      <View style={[styles.item, {borderLeftColor: paper.getColor()}]}>
        <Text style={styles.itemTitle}>{_.trunc(paper.title, 100)}</Text>
        <Text style={styles.itemSubtext}>{this._getAuthorsDisplay(paper.authors)}</Text>
      </View>
    </TouchableHighlight>
  }
}
