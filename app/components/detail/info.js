'use strict';

import React from 'react-native';
import _ from 'lodash';
import { Icon } from 'react-native-icons';
import moment from 'moment';

import { getLabelForCategory } from '../../utils';
import styles from './styles';

const {
  Component, Text, View,
} = React;


export default class Info extends Component {
  render() {
    const { paper } = this.props;
    let comment;
    
    if (paper.comment) {
      comment = <View style={styles.infoRow}>
        <Icon
          name='ion|android-textsms'
          size={15}
          color='#666'
          style={styles.infoIcon}
        />
        <Text style={styles.infoText}>
          {paper.comment}
        </Text>
      </View>
    }

    return <View style={styles.info}>
      <View style={styles.infoRow}>
        <Icon
          name='ion|android-calendar'
          size={15}
          color='#666'
          style={styles.infoIcon}
        />
        <Text style={styles.infoText}>
          published {moment(paper.published).format('Do MMM YYYY')}
          {paper.updated && paper.updated !== paper.published
           ? `, updated ${moment(paper.updated).format('Do MMM YYYY')}`
           : ''}
        </Text>
      </View>

      {comment}

      <View style={styles.infoRow}>
        <Icon
          name='ion|android-folder'
          size={15}
          color='#666'
          style={styles.infoIcon}
        />
        <Text style={styles.infoText}>
          {_.map(paper.categories, getLabelForCategory).join(', ')}
        </Text>
      </View>
    </View>
  }
}
