'use strict';

import React from 'react-native';
import _ from 'lodash';

import styles from './styles';

const {
  Component, Text, View,
  TouchableOpacity
} = React;


export default class Header extends Component {
  render() {
    const { paper } = this.props;

    return <View style={[styles.header, {backgroundColor: paper.getColor()}]}>
      <View style={styles.headerContent}>
        <Text style={styles.title}>{paper.title}</Text>
        <Text style={styles.subtitle}>
          by {paper.authors.join(', ')}
        </Text>
      </View>
    </View>;
  }
}
