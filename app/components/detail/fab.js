'use strict';

import React from 'react-native';
import _ from 'lodash';
import { Icon } from 'react-native-icons';
import { Circle } from 'react-native-material-paper';

import TouchableFeedback from '../common/touchable';
import styles from './styles';

const {
  Component, Text, View,
} = React;


export default class FAB extends Component {
  render() {
    return <Circle
        style={styles.fab}
        radius={28}
        color='#1AB2C3'
        elevation={3}>
      <TouchableFeedback
          onPress={() => setTimeout(this.props.onPress, 100)}
          color='#106973'>
        <View style={styles.fabContent}>
          <Icon
            name='ion|document-text'
            size={25}
            color='#FFF'
            style={styles.navIcon}
          />
        </View>
      </TouchableFeedback>
    </Circle>
  }
}
