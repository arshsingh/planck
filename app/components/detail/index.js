'use strict';

import React from 'react-native';
import _ from 'lodash';
import NavBar from 'react-native-material-navbar';
import { Icon } from 'react-native-icons';

import * as Actions from '../../actions';
import TouchableFeedback from '../common/touchable';
import Reader from '../reader';
import Header from './header';
import FAB from './fab';
import Info from './info';
import styles from './styles';

const {
  Component,
  Text,
  View,
  ScrollView,
  Platform
} = React;


export default class Detail extends Component {
  static get sceneId() { return 'detail'; }

  render() {
    const { paper, dispatch, navigator } = this.props;

    if (_.isEmpty(paper)) {
      return <Text>
        Loading...
      </Text>
    }

    // Left button on the NavBar
    const leftNav = <TouchableFeedback
        color='#666'
        onPress={() => navigator.pop()}>
      <View style={styles.navBtn}>
        <Icon
          name='ion|android-arrow-back'
          size={25}
          color='#FFF'
          style={styles.navIcon}
        />
      </View>
    </TouchableFeedback>

    return <View style={styles.container}>
      <NavBar
        left={leftNav}
        statusBarColorAndroid='#333333'
      />

      <ScrollView>
        <Header paper={paper}/>
        <Info paper={paper}/>
        <Text style={styles.summary}>{paper.summary}</Text>
      </ScrollView>

      <FAB
        onPress={() => navigator.push({
          name: _.trunc(paper.title, 35),
          id: Reader.sceneId,
          props: { save: false }
        })}
      />
    </View>
  }
}
