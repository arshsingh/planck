'use strict';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    flex: 1
  },
  header: {
    padding: 10,
    paddingTop: 5,
  },
  headerContent: {
    padding: 15
  },
  title: {
    color: '#FFF',
    fontSize: 18,
    fontWeight: 'bold'
  },
  subtitle: {
    color: '#FFF',
    fontSize: 12
  },
  summary: {
    marginHorizontal: 20,
    color: '#666',
    fontSize: 12,
    lineHeight: 20,
    paddingBottom: 80
  },
  navBtn: {
    width: 35,
    height: 35,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  navIcon: {
    height: 25,
    width: 25,
    opacity: 0.8
  },
  fabIcon: {
    height: 30,
    width: 30,
  },
  info : {
    margin: 20,
    borderBottomColor: '#EEE',
    borderBottomWidth: 0.3,
    paddingBottom: 15
  },
  infoRow: {
    flexDirection: 'row',
    paddingVertical: 5
  },
  infoText: {
    fontSize: 12,
    flex: 1
  },
  infoIcon: {
    height: 16,
    width: 16,
    marginRight: 5
  },
  fab: {
    position: 'absolute',
    bottom: 30,
    right: 30,
  },
  fabContent: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
