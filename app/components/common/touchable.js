'use strict';

import React from 'react-native';

const {
	PropTypes,
  Component,
  View,
  Platform,
  TouchableNativeFeedback,
  TouchableOpacity
} = React;


export default class TouchableFeedback extends Component {
  static propTypes = {
    color: PropTypes.string,
    rippleBorderless: PropTypes.bool,
    activeOpacity: PropTypes.number,
    onPress: PropTypes.func.isRequired
  }

  static defaultProps = {
    color: '#AAA',
    rippleBorderless: true,
    activeOpacity: 0.7
  }

  render() {
    if (Platform.OS === 'android') {
      return (
        <TouchableNativeFeedback
          delayPressIn={0}
          children={this.props.children}
          background={TouchableNativeFeedback.Ripple(
            this.props.color, this.props.rippleBorderless)}
          onPress={this.props.onPress}
        />
      )
    }

    return (
      <TouchableOpacity
        children={this.props.children}
        activeOpacity={this.props.activeOpacity}
        onPress={this.props.onPress}/>
    )
  }
}