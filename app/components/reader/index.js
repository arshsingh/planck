'use strict';

import React from 'react-native';
import RNFS from 'react-native-fs';
import _ from 'lodash';

import * as Actions from '../../actions';
import PDF from './pdf';
import styles from './styles';

const {
  Component,
  View,
  Text,
  Platform
} = React;


export default class Reader extends Component {
  static get sceneId() { return 'reader'; }

  constructor(props){
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.checkFile();
  }

  componentDidUpdate(prevProps) {
    // downloading has finished
    if (prevProps.downloading && !this.props.downloading) {
      this.checkFile();
    }
  }

  checkFile() {
    const { paper, dispatch } = this.props;

    if (Platform.OS === 'ios' && save !== true) {
      this.setState({ file: paper.pdf });
    } else {
      const file = paper._file || (
        `${RNFS.DocumentDirectoryPath}/tmp/${paper.id}.pdf`
      );

      RNFS.stat(file).then(something => {
        this.setState({ file });
      }).catch((err) => {
        // file doesn't exist, download again
        dispatch(Actions.downloadPaper(paper, Boolean(paper._file)));
      });
    }
  }

  render() {
    const { paper, downloading } = this.props;

    if (!this.state.file) {
      let progress = downloading && downloading.progress || {};
      let total = progress.contentLength || 0;
      let written = progress.bytesWritten || 0;

      return (
        <View style={styles.progressOverlay}>
          <Text style={styles.progressText}>
            { written > 0
              ? `${Math.round((written / (total || 1)) * 100)} %`
              : 'Loading' }
          </Text>
        </View>
      )
    }

    return <PDF file={this.state.file}/>
  }
}
