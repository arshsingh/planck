'use strict';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  progressOverlay: {
    backgroundColor: '#FFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  progressText: {
    fontSize: 36,
    color: '#CCC',
    fontFamily: 'sans-serif-thin'
  }
});
