'use strict';

import React from 'react-native';
import PDFView from 'react-native-pdf-view';

const {
  Component,
  WebView
} = React;


export default class Reader extends Component {
  render() {
    const { file } = this.props;

    return (
      <PDFView ref={(pdf)=>{this.pdfView = pdf;}}
        src={file}
        onLoadComplete = {() => {}}
        style={{flex: 1}}/>
    )
  }
}
