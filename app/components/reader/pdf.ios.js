'use strict';

import React from 'react-native';

const {
  Component,
  WebView
} = React;


export default class Reader extends Component {
  render() {
    const { file } = this.props;

    return (
      <WebView
        url={file}
        javaScriptEnabledAndroid={true}
      />
    )
  }
}
