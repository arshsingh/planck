'use strict';

export default [
  // Statistics
  {
    id: 'stat', query: 'stat.*', label: 'Statistics',
    children: [
      {id: 'stat.AP', query: 'stat.AP', label: 'Applications'},
      {id: 'stat.CO', query: 'stat.CO', label: 'Computation'},
      {id: 'stat.ML', query: 'stat.ML', label: 'Machine Learning'},
      {id: 'stat.ME', query: 'stat.ME', label: 'Methodology'},
      {id: 'stat.TH', query: 'stat.TH', label: 'Theory'},         
    ]
  },
  // Quantitative Biology
  {
    id: 'q-bio', query: 'q-bio.*', label: 'Quantitative Biology',
    children: [
      {id: 'q-bio.BM', query: 'q-bio.BM', label: 'Biomolecules'},
      {id: 'q-bio.CB', query: 'q-bio.CB', label: 'Cell Behavior'},
      {id: 'q-bio.GN', query: 'q-bio.GN', label: 'Genomics'},
      {id: 'q-bio.MN', query: 'q-bio.MN', label: 'Molecular Networks'},
      {id: 'q-bio.NC', query: 'q-bio.NC', label: 'Neurons and Cognition'},
      {id: 'q-bio.PE', query: 'q-bio.PE', label: 'Populations and Evolution'},
      {id: 'q-bio.QM', query: 'q-bio.QM', label: 'Quantitative Methods'},
      {id: 'q-bio.SC', query: 'q-bio.SC', label: 'Subcellular Processes'},
      {id: 'q-bio.TO', query: 'q-bio.TO', label: 'Tissues and Organs'},
      {id: 'q-bio.OT', query: 'q-bio.OT', label: 'Other'},
    ]
  },
  // Computer Science
  {
    id: 'cs', query: 'cs.*', label: 'Computer Science',
    children: [
      {id: 'cs.AR', query: 'cs.AR', label: 'Architecture'},
      {id: 'cs.AI', query: 'cs.AI', label: 'Artificial Intelligence'},
      {id: 'cs.CL', query: 'cs.CL', label: 'Computation and Language'},
      {id: 'cs.CC', query: 'cs.CC', label: 'Computational Complexity'},
      {id: 'cs.CE', query: 'cs.CE', label: 'Computational Engineering; Finance; and Science'},
      {id: 'cs.CG', query: 'cs.CG', label: 'Computational Geometry '},
      {id: 'cs.GT', query: 'cs.GT', label: 'Computer Science and Game Theory'},
      {id: 'cs.CV', query: 'cs.CV', label: 'Computer Vision and Pattern Recognition'},
      {id: 'cs.CY', query: 'cs.CY', label: 'Computers and Society'},
      {id: 'cs.CR', query: 'cs.CR', label: 'Cryptography and Security'},
      {id: 'cs.DS', query: 'cs.DS', label: 'Data Structures and Algorithms'},
      {id: 'cs.DB', query: 'cs.DB', label: 'Databases'},
      {id: 'cs.DL', query: 'cs.DL', label: 'Digital Libraries'},
      {id: 'cs.DM', query: 'cs.DM', label: 'Discrete Mathematics'},
      {id: 'cs.DC', query: 'cs.DC', label: 'Distributed; Parallel; and Cluster Computing'},
      {id: 'cs.GL', query: 'cs.GL', label: 'General Literature'},
      {id: 'cs.GR', query: 'cs.GR', label: 'Graphics'},
      {id: 'cs.HC', query: 'cs.HC', label: 'Human-Computer Interaction'},
      {id: 'cs.IR', query: 'cs.IR', label: 'Information Retrieval'},
      {id: 'cs.IT', query: 'cs.IT', label: 'Information Theory'},
      {id: 'cs.LG', query: 'cs.LG', label: 'Learning'},
      {id: 'cs.LO', query: 'cs.LO', label: 'Logic in Computer Science'},
      {id: 'cs.MS', query: 'cs.MS', label: 'Mathematical Software'},
      {id: 'cs.MA', query: 'cs.MA', label: 'Multiagent Systems'},
      {id: 'cs.MM', query: 'cs.MM', label: 'Multimedia'},
      {id: 'cs.NI', query: 'cs.NI', label: 'Networking and Internet Architecture'},
      {id: 'cs.NE', query: 'cs.NE', label: 'Neural and Evolutionary Computing'},
      {id: 'cs.NA', query: 'cs.NA', label: 'Numerical Analysis'},
      {id: 'cs.OS', query: 'cs.OS', label: 'Operating Systems'},
      {id: 'cs.PF', query: 'cs.PF', label: 'Performance'},
      {id: 'cs.PL', query: 'cs.PL', label: 'Programming Languages'},
      {id: 'cs.RO', query: 'cs.RO', label: 'Robotics'},
      {id: 'cs.SE', query: 'cs.SE', label: 'Software Engineering'},
      {id: 'cs.SD', query: 'cs.SD', label: 'Sound'},
      {id: 'cs.SC', query: 'cs.SC', label: 'Symbolic Computation'},
      {id: 'cs.OH', query: 'cs.OH', label: 'Other'},
    ]
  },
  // Nonlinear Sciences
  {
    id: 'nlin', query: 'nlin.*', label: 'Nonlinear Sciences',
    children: [
      {id: 'nlin.AO', query: 'nlin.AO', label: 'Adaptation and Self-Organizing Systems'},
      {id: 'nlin.CG', query: 'nlin.CG', label: 'Cellular Automata and Lattice Gases'},
      {id: 'nlin.CD', query: 'nlin.CD', label: 'Chaotic Dynamics'},
      {id: 'nlin.SI', query: 'nlin.SI', label: 'Exactly Solvable and Integrable Systems'},
      {id: 'nlin.PS', query: 'nlin.PS', label: 'Pattern Formation and Solitons'},
    ]
  },
  // Mathematics
  {
    id: 'math', query: 'math.*', label: 'Mathematics',
    children: [
      {id: 'math.AG', query: 'math.AG', label: 'Algebraic Geometry'},
      {id: 'math.AT', query: 'math.AT', label: 'Algebraic Topology'},
      {id: 'math.AP', query: 'math.AP', label: 'Analysis of PDEs'},
      {id: 'math.CT', query: 'math.CT', label: 'Category Theory'},
      {id: 'math.CA', query: 'math.CA', label: 'Classical Analysis and ODEs'},
      {id: 'math.CO', query: 'math.CO', label: 'Combinatorics'},
      {id: 'math.AC', query: 'math.AC', label: 'Commutative Algebra'},
      {id: 'math.CV', query: 'math.CV', label: 'Complex Variables'},
      {id: 'math.DG', query: 'math.DG', label: 'Differential Geometry'},
      {id: 'math.DS', query: 'math.DS', label: 'Dynamical Systems'},
      {id: 'math.FA', query: 'math.FA', label: 'Functional Analysis'},
      {id: 'math.GM', query: 'math.GM', label: 'General Mathematics'},
      {id: 'math.GN', query: 'math.GN', label: 'General Topology'},
      {id: 'math.GT', query: 'math.GT', label: 'Geometric Topology'},
      {id: 'math.GR', query: 'math.GR', label: 'Group Theory'},
      {id: 'math.HO', query: 'math.HO', label: 'History and Overview'},
      {id: 'math.IT', query: 'math.IT', label: 'Information Theory'},
      {id: 'math.KT', query: 'math.KT', label: 'K-Theory and Homology'},
      {id: 'math.LO', query: 'math.LO', label: 'Logic'},
      {id: 'math.MP', query: 'math.MP', label: 'Mathematical Physics'},
      {id: 'math.MG', query: 'math.MG', label: 'Metric Geometry'},
      {id: 'math.NT', query: 'math.NT', label: 'Number Theory'},
      {id: 'math.NA', query: 'math.NA', label: 'Numerical Analysis'},
      {id: 'math.OA', query: 'math.OA', label: 'Operator Algebras'},
      {id: 'math.OC', query: 'math.OC', label: 'Optimization and Control'},
      {id: 'math.PR', query: 'math.PR', label: 'Probability'},
      {id: 'math.QA', query: 'math.QA', label: 'Quantum Algebra'},
      {id: 'math.RT', query: 'math.RT', label: 'Representation Theory'},
      {id: 'math.RA', query: 'math.RA', label: 'Rings and Algebras'},
      {id: 'math.SP', query: 'math.SP', label: 'Spectral Theory'},
      {id: 'math.ST', query: 'math.ST', label: 'Statistics'},
      {id: 'math.SG', query: 'math.SG', label: 'Symplectic Geometry'},
    ]
  },
  // Astrophysics
  {
    id: 'astro-ph', query: 'astro-ph.*', label: 'Astrophysics',
    children: [
      {id: 'astro-ph.GA', query: 'astro-ph.GA', label: 'Astrophysics of Galaxies'},
      {id: 'astro-ph.CO', query: 'astro-ph.CO', label: 'Cosmology and Nongalactic Astrophysics'},
      {id: 'astro-ph.EP', query: 'astro-ph.EP', label: 'Earth and Planetary Astrophysics'},
      {id: 'astro-ph.HE', query: 'astro-ph.HE', label: 'High Energy Astrophysical Phenomena'},
      {id: 'astro-ph.IM', query: 'astro-ph.IM', label: 'Instrumentation and Methods for Astrophysics'},
      {id: 'astro-ph.SR', query: 'astro-ph.SR', label: 'Solar and Stellar Astrophysics'},
    ]
  },
  // Condensed Matter Physics
  {
    id: 'cond-mat', query: 'cond-mat.*', label: 'Condensed Matter Physics',
    children: [
      {id: 'cond-mat.dis-nn', query: 'cond-mat.dis-nn', label: 'Disordered Systems and Neural Networks'},
      {id: 'cond-mat.mes-hall', query: 'cond-mat.mes-hall', label: 'Mesoscopic Systems and Quantum Hall Effect'},
      {id: 'cond-mat.mtr-sci', query: 'cond-mat.mtr-sci', label: 'Materials Science'},
      {id: 'cond-mat.soft', query: 'cond-mat.soft', label: 'Soft Condensed Matter'},
      {id: 'cond-mat.stat-mech', query: 'cond-mat.stat-mech', label: 'Statistical Mechanics'},
      {id: 'cond-mat.str-el', query: 'cond-mat.str-el', label: 'Strongly Correlated Electrons'},
      {id: 'cond-mat.super-con', query: 'cond-mat.super-con', label: 'Superconductivity'},
      {id: 'cond-mat.other', query: 'cond-mat.other', label: 'Other'},
    ]
  },
  // General Relativity and Quantum Cosmology
  {
    id: 'gr-qc', query: 'gr-qc', label: 'General Relativity and Quantum Cosmology'
  },
  // High Energy Physics
  {
    id: 'hep', query: 'hep-*', label: 'High Energy Physics',
    color: '#9383D4',
    children: [
      {id: 'hep-ex', query: 'hep-ex', label: 'Experiment'},
      {id: 'hep-lat', query: 'hep-lat', label: 'Lattice'},
      {id: 'hep-ph', query: 'hep-ph', label: 'Phenomonology'},
      {id: 'hep-th', query: 'hep-th', label: 'Theory'},
    ]
  },
  // Mathematical Physics
  {
    id: 'math-ph', query: 'math-ph', label: 'Mathematical Physics'
  },
  // Nuclear Physics
  {
    id: 'nucl', query: 'nucl-*', label: 'Nuclear Physics',
    children: [
      {id: 'nucl-ex', query: 'nucl-ex', label: 'Experiment'},
      {id: 'nucl-th', query: 'nucl-th', label: 'Theory'},
    ]
  },
  // Quantum Physics
  {
    id: 'quant-ph', query: 'quant-ph', label: 'Quantum Physics'
  },
  // Physics
  {
    id: 'physics', query: 'physics.*', label: 'Physics',
    children: [
      {id: 'physics.acc-ph', query: 'physics.acc-ph', label: 'Accelerator Physics'},
      {id: 'physics.ao-ph', query: 'physics.ao-ph', label: 'Atmospheric and Oceanic Physics'},
      {id: 'physics.atom-ph', query: 'physics.atom-ph', label: 'Atomic Physics'},
      {id: 'physics.atm-clus', query: 'physics.atm-clus', label: 'Atomic and Molecular Clusters'},
      {id: 'physics.bio-ph', query: 'physics.bio-ph', label: 'Biological Physics'},
      {id: 'physics.chem-ph', query: 'physics.chem-ph', label: 'Chemical Physics'},
      {id: 'physics.class-ph', query: 'physics.class-ph', label: 'Classical Physics'},
      {id: 'physics.comp-ph', query: 'physics.comp-ph', label: 'Computational Physics'},
      {id: 'physics.data-an', query: 'physics.data-an', label: 'Data Analysis; Statistics and Probability'},
      {id: 'physics.flu-dyn', query: 'physics.flu-dyn', label: 'Fluid Dynamics'},
      {id: 'physics.gen-ph', query: 'physics.gen-ph', label: 'General Physics'},
      {id: 'physics.geo-ph', query: 'physics.geo-ph', label: 'Geophysics'},
      {id: 'physics.hist-ph', query: 'physics.hist-ph', label: 'History of Physics'},
      {id: 'physics.ins-det', query: 'physics.ins-det', label: 'Instrumentation and Detectors'},
      {id: 'physics.med-ph', query: 'physics.med-ph', label: 'Medical Physics'},
      {id: 'physics.opt-ph', query: 'physics.opt-ph', label: 'Optics'},
      {id: 'physics.ed-ph', query: 'physics.ed-ph', label: 'Physics Education'},
      {id: 'physics.soc-ph', query: 'physics.soc-ph', label: 'Physics and Society'},
      {id: 'physics.plasm-ph', query: 'physics.plasm-ph', label: 'Plasma Physics'},
      {id: 'physics.pop-ph', query: 'physics.pop-ph', label: 'Popular Physics'},
      {id: 'physics.space-ph', query: 'physics.space-ph', label: 'Space Physics'},
    ]
  },
];
